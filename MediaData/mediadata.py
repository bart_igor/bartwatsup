#!/usr/bin/python
import sys
import hashlib
import base64

hash = hashlib.sha256()
filein = open(sys.argv[1],'rb')
data = filein.read()
hash.update(data)
thumb=b'thumb'
size = len(data)
out = thumb + b'|' + base64.encodestring(data) + b'|' + hash.hexdigest() + b'|' + str(size)
sys.stdout.write(out)