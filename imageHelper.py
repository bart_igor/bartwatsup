import os
import hashlib
import time
from subprocess import Popen, PIPE




def hashImage(fname):
    hasher = hashlib.sha256()
    with open(fname, 'rb') as afile:
        buf = afile.read()
        hasher.update(buf)
    return hasher.hexdigest()

def sizeImage(fname):
    b = os.path.getsize(fname)
    return b


def createNameImage():
    date = time.strftime("%Y%m%d")
    tim = time.strftime("%H%M")
    path = "/sdcard/WhatsApp/Media/WhatsApp Images/Sent/IMG-"+date+"-WA"+tim+".jpg"
    return path
    

def imageGetCode(imgName,orgName):
    cmd = "MediaData\MediaData.exe \""+imgName+"\" "+orgName
    b = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE).stdout.read()
    data = b.replace("\r\n","")
    data = data.split('|')
    return data


def imageGetCode_py(imgName,orgName):
    cmd = os.path.join("MediaData","mediadata.py")+" "+imgName+" "+orgName
    b = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE).stdout.read()
    data = b.replace("\r\n","")
    data = data.split('|')
    return data
