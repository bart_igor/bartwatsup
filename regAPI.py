#!/usr/bin/python
# coding: utf8
import time
import re
import pymysql
import os
import adb
import paramiko
import shutil
import urllib
from subprocess import Popen, PIPE
from multiprocessing import Process, freeze_support


if os.name=='posix':
    adb_command ='adb'
else:
    adb_command ='res\\adb.exe'
connection_string = {"host":"127.0.0.1", "user":"root", "passwd": "root", "db": "wapp", "charset": "utf8"}


def getNumbers():
    api = "http://sms-activate.ru/stubs/handler_api.php?api_key=2bA974bcbf68b795dA6d43f7ed3be1e7&action=getNumber&service=wa"
    response = urllib.urlopen(api)
    data = response.read().split(':')
    try:
        return data[2],data[1]
    except IndexError:
        print "NETU NOMEROV"
        exit()

	
def getMachines():
    machineList = open("machines.list", "r")
    machines = machineList.read().split("\n")
    return machines


class StartREG(Process):
    def __init__(self,numbers, dev):
        self.numbers = numbers
        self.dev = dev
        Process.__init__(self)
    def run(self):
        reg(self.numbers,self.dev)



class Stack:
     def __init__(self):
         self.items = []

     def isEmpty(self):
         return self.items == []

     def push(self, item):
         self.items.append(item)

     def pop(self):
         return self.items.pop()

     def peek(self):
         return self.items[len(self.items)-1]

     def size(self):
         return len(self.items)

class Reg:
     __devsReg = None;
     def __init__(self,numbers,devs):
          self.numbers = numbers
          self.devs = devs
          self.__delezhka()

     def __delezhka(self):
       celoe,ostatok = divmod(len(self.numbers),len(self.devs))
       countDevs = range(len(self.devs))
       self.__devsReg = countDevs
       stack = Stack()

       for num in self.numbers:
          stack.push(num)
       numbersDev = []

       for i in range(len(countDevs)):
          if(i==len(countDevs)-1):
               it = celoe + ostatok
          else:
               it = celoe

          for j in range(it):
            numbersDev.append(stack.pop())
          self.__devsReg[i] = numbersDev
          numbersDev = []



     def printNumbers(self):
         print self.__devsReg
         print self.devs


     def startReg(self):
          i = 0
          th = range(len(self.__devsReg))
          for order in self.__devsReg:
               th[i] = StartREG(order,self.devs[i])
               th[i].start()
               i = i+1
        

		
def writeNumber(db,number,flag):
    sql = db.cursor()
    sql.execute("insert into reg2 (number) values ('"+number+"');")
    db.commit()
	
def updateNumber(db,number,flag):
    sql = db.cursor()
    sql.execute("update reg2 set flag='"+flag+"',date_activate=NOW() where number='"+number+"' ")
    db.commit()
		
def writeChannel(dev,db,number):
    print "Channel write"
    saveChannel(dev,number)
    sql = db.cursor()
    device = dev.split(':')
    dev = device[0]
    dev = dev[11:]
    sql.execute("insert into channels (dev,number,flag) values ('"+dev+"','"+number+"','1');")
    db.commit()
	
def saveChannel(dev,account):
    if(os.path.exists(os.path.join("channels", account))):
        shutil.rmtree(os.path.join("channels", account))
    os.mkdir(os.path.join("channels", account))
    sendCommand(dev,"\'chmod -R 777 /data/data/com.whatsapp\'")
    sendCommand(dev,"\'sh /sdcard/WApp/scripts/tar.sh\'")
    runCmd(adb_command+" -s "+dev+" cpull /data/data/com.whatsapp/channel.tar "+os.path.join("channel",account))

def runCmd(cmd):
    print "cmd: ",cmd
    pid_start = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE)
    shell = pid_start.stdout.read()
    print "recived cmd:",shell
    if(len(shell)>2000):
        print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    pid_start.terminate()
    return shell

def sendCommand(dev,command):
    dev = dev.replace('\r','')
    command = adb_command+' -s '+dev+' shell "su -c '+command+'"'
    out = runCmd(command)
    print command 
    return out


	
def getCode(id_number):
    status = "http://sms-activate.ru/stubs/handler_api.php?api_key=2bA974bcbf68b795dA6d43f7ed3be1e7&action=setStatus&status=1&id="+id_number
    response = urllib.urlopen(status)
    time.sleep(1)
    api = "http://sms-activate.ru/stubs/handler_api.php?api_key=2bA974bcbf68b795dA6d43f7ed3be1e7&action=getStatus&id="+id_number
    timE = time.time()
    data = ""
    while(len(data.split(':'))==1 and time.time()-timE<130):
        print time.time()-timE
        time.sleep(10)
        response = urllib.urlopen(api)
        data = response.read()
        print data
        if(len(data.split(':'))!=1):
            status = "http://sms-activate.ru/stubs/handler_api.php?api_key=2bA974bcbf68b795dA6d43f7ed3be1e7&action=setStatus&status=6&id="+id_number
            response = urllib.urlopen(status)
            dataS = response.read().split(":")
            codeD = data.split(':')
            return codeD[1]
    return ""
    

def startWhatsapp(dev,bol):
    sendCommand(dev,"\'monkey -p com.whatsapp -c android.intent.category.LAUNCHER 1\'")
    if(bol!="no"):
        waitWhatsappActive(dev)

def waitWhatsappActive(dev):
    out = sendCommand(dev,"\'dumpsys window windows | grep mCurrentFocus\'")
    while(re.search('EULA',out)!=None):
        out = sendCommand(dev,"\'dumpsys window windows | grep mCurrentFocus\'")

def startProxy(client,LoPort,PrIP,PrPort,PrType):
	client.exec_command("sh /usr/redsocks/redsocks.sh start "+LoPort+" "+PrIP+" "+PrPort+" "+PrType)
def stopProxy(client,LoPort):
	client.exec_command("sh /usr/redsocks/redsocks.sh stop "+LoPort)

	
	
def reconnectModem(dev,client):
    ip = dev.split(':')
    mac = ip[0][len(ip[0])-3:len(ip[0])]
    client.exec_command("/home/mbm/changeip.sh 192.168.10."+mac)
    time.sleep(300)

	
def reg(numbers,dev):
    adb.sendCommand(dev,"\'echo 3 > /proc/sys/vm/drop_caches\'")
    db = pymysql.connect(**connection_string)
    host = '192.168.10.1'
    user = 'root'
    passw = 'root'
    port = 22
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(hostname=host, username=user, password=passw, port=port)
    chCount = 0
    while(True):
        chCount = chCount + 1
        number,id_number = getNumbers()
        if(chCount>5):
            chCount = 0
            reconnectModem(dev,client)		
        sendCommand(dev,"\'am force-stop com.android.browser\'")
        time.sleep(0.2)
        sendCommand(dev,"\'pm clear com.android.browser\'")
        time.sleep(0.2)
        sendCommand(dev,"\'rm /sdcard/WhatsApp/Databases/*\'")
        sendCommand(dev,"\'pm clear com.whatsapp\'")
        print "SET ANDROID_ID"
        adb.changeId(dev)
        time.sleep(0.5)
        adb.sendCommand(dev,"\'echo 3 > /proc/sys/vm/drop_caches\'")
        startWhatsapp(dev,"")
        time.sleep(3)
        sendCommand(dev,"\'input tap 111 217\'")
        time.sleep(1)
        sendCommand(dev,"\'input tap 124 262\'")
        time.sleep(1)
        sendCommand(dev,"\'input tap 119 232\'")
        time.sleep(0.5)
        sendCommand(dev,"\'input tap 79 89\'")
        time.sleep(0.5)
        sendCommand(dev,"\'input text 7\'")
        time.sleep(0.2)
        sendCommand(dev,"\'input tap 63 188\'")
        time.sleep(0.5)
        sendCommand(dev,"\'input text "+ number[1:len(number)]+"\'")
        time.sleep(0.4)
        sendCommand(dev,"\'input tap 200 269\'")
        writeNumber(db,number,"1")
        bban = False
        seR = False 
        while(adb.checkImg(dev)!="OK"):
            time.sleep(1)
            if(adb.checkImg(dev)=="BAN"):
                print "NUMBER IS BAN"
                bban = True
                break
            if(adb.checkImg(dev)=="SER"):
                seR = True
                break
        if(bban):
            continue
        if(not seR):
            sendCommand(dev,"\'input tap 191 206\'") #Knopka OK posle vvoda nomera
            time.sleep(3)
            code = getCode(id_number)
            if(code==""):
                updateNumber(db,number,"0")
                print "SIMKY NA POVTOR"
                continue
            updateNumber(db,number,"2")
            time.sleep(2)
            sendCommand(dev,"\'am start -a android.intent.action.VIEW -d https://v.whatsapp.com/"+code+"\'")
            time.sleep(4)
        else:
            sendCommand(dev,"\'input tap 490 334\'")
        seR2 = False
        while(adb.checkImg(dev)!="WIN"):
            time.sleep(1)
            if(adb.checkImg(dev)=="BAN"):
                print "NUMBER IS BAN"
                bban = True
                break
            if(adb.checkImg(dev)=="CALL"):
                print "NUMBER IS BAN"
                print "SIMKY NA POVTOR"
                bban = True
                break
            if(adb.checkImg(dev)=="SER"):
                seR2 = True
                break
        if(bban):
            continue
        if(seR2):
             sendCommand(dev,"\'input tap 490 334\'")
        time.sleep(2)
        print "WINDOW OPEN"
        sendCommand(dev,"\'input tap 53 174\'")
        time.sleep(0.5)
        sendCommand(dev,"\'input text Ivan\'")
        time.sleep(0.5)
        sendCommand(dev,"\'input tap 200 36\'")
        time.sleep(4)
        while(adb.checkImg(dev)!="DALEE"):
            if(adb.checkImg(dev)=="LOST"):
                sendCommand(dev,"\'input tap 178 197\'")
                time.sleep(1)
                sendCommand(dev,"\'input tap 200 36\'")
                time.sleep(1)
        sendCommand(dev,"\'input tap 92 254\'")
        if(seR2):
            while(adb.checkImg(dev)!="BACKUP"):
                time.sleep(1)
            time.sleep(0.5)
            sendCommand(dev,"\'input tap 68 328\'")
            time.sleep(0.5)
            sendCommand(dev,"\'input tap 743 534\'")
            time.sleep(7)
        if(seR):
            while(adb.checkImg(dev)!="BACKUP"):
                time.sleep(1)
            sendCommand(dev,"\'input tap 60 322\'")
            time.sleep(1)
        writeChannel(dev, db, number)
    db.close()

if __name__ == '__main__':
    freeze_support()	
    start = Reg(getNumbers(),getMachines())
    start.startReg()
