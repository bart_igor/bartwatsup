#!/usr/bin/python
# -*- coding: utf-8 -*-
import adb
import pymysql
import time
import re
import os
from random import randint
from multiprocessing import Process, freeze_support

connection_string = {"host":"127.0.0.1", "user":"root", "passwd": "root", "db": "wapp", "charset": "utf8"}
if os.name=='posix':
    adb_command ='adb'
else:
    adb_command ='res\\adb.exe'

def writeChannel(dev,db,number):
    adb.saveChannel(dev,number)
    sql = db.cursor()
    sql.execute("insert into channels (number,flag) values ('"+number+"','1');")
    db.commit()
	
def startProxy(client,dev,LoPort,PrIP,PrPort,PrType):
    client.exec_command("sh /usr/redsocks/redsocks.sh start "+LoPort+" "+PrIP+" "+PrPort+" "+PrType)
    print "sh /usr/redsocks/redsocks.sh start "+LoPort+" "+PrIP+" "+PrPort+" "+PrType
    time.sleep(0.3)
    ip = adb.getIP(dev)
    client.exec_command("iptables -t nat -A PREROUTING --src "+ip+" -p tcp -j REDIRECT --to-ports "+LoPort)
def stopProxy(client,dev,LoPort):
    client.exec_command("sh /usr/redsocks/redsocks.sh stop "+LoPort)
    time.sleep(0.3)
    ip = adb.getIP(dev)
    client.exec_command("iptables -t nat -D PREROUTING --src "+ip+" -p tcp -j REDIRECT --to-ports "+LoPort)
	

def setInfoChannel(db,flag,number):
    number = str(number)
    sql = db.cursor()
    sql.execute("update channels set flag='"+flag+"',time_ban=CURRENT_TIMESTAMP where number='"+number+"'")
    db.commit()    
	
def setChannelTotal(db,count,number):
    sql = db.cursor()
    sql.execute("select count_text from channels where number='"+number+"'")
    data =  sql.fetchall()
    if(data==""):
        return False
    try:
        count = count + int(data[0][0])
        sql.execute("update channels set count_text='"+str(count)+"' where number='"+number+"'")
        db.commit()
    except IndexError:
        print ""

def getChannelsCount(channels):
    return len(channels)
	
def deleteChannel(db,number):
    number = str(number)
    sql = db.cursor()
    sql.execute("delete from channels where number='"+number+"'")
    db.commit()       

def editPhone(dev,number):
    adb.sendCommand(dev,"\'monkey -p com.vivek.imeichangerpro -c android.intent.category.LAUNCHER 1\'")	
    time.sleep(1)	
    adb.sendCommand(dev,"\'input tap 121 240\'")	
    time.sleep(0.3)
    adb.sendCommand(dev,"\'monkey -p org.lytsing.myphonenumber -c android.intent.category.LAUNCHER 1\'")	
    time.sleep(1)	
    adb.sendCommand(dev,"\'input tap 107 123\'")	
    time.sleep(0.3)
    adb.sendCommand(dev,"\'input text "+ "380"+number+"\'")
    time.sleep(0.3)
    adb.sendCommand(dev,"\'input tap 59 211\'")	       
    time.sleep(0.3)
    adb.sendCommand(dev,"\'input keyevent KEYCODE_BACK\'")	
	
	
def send(dev,db,numbers,proxy,codes,message,pathImage,pathAva,name,orderId):
    #time.sleep(30)     ####zaderchka
    channels = getChannels(db,dev)
    channel = 0	
    fail = False
    counter = 0
    countChan = len(channels)
    print str(countChan) + "CCCCCCCCCCCCCCC" + dev
    ccount = 0
    numsIsEmpty = False
    print str(countChan) + "  COUNT CHANNELS"
    adb.sendCommand(dev,"\'echo 3 > /proc/sys/vm/drop_caches\'")
    #print "reconect adb for dev:",dev
    #adb.disconnect(dev=dev)
    #time.sleep(0.3)
    #adb.connect(dev=dev)
    while(not numsIsEmpty and countChan != 0):   #Условие выхода
	#init
	#time sleep (2min)
        account = channels[channel]
        print str(account) +"-"+ dev + " NUMBER <--> DEVICE"
        account = str(account)
        if(account!=""):
            setInfoChannel(db,"2",account)
        editPhone(dev,account)
        channel = channel + 1
        ###################rec
        for iChannel in range(29):  #количество проходов по каналу
            print "NEW SEND FIVE MESSAGE " + str(iChannel)
            nums = []
            for iNum in range(1):  #количество отправленных с канала за проход
                try:
				    nums.append(numbers.pop())
                except IndexError:
                    print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                    numsIsEmpty = True				
            adb.stopWhatsapp(dev)
            adb.cleanContact(dev)
            adb.cleanChannel(dev)
            adb.changeId(dev)
            time.sleep(0.5)
            adb.restoreChannel(dev,account)
            adb.startWhatsapp(dev,"")
            time.sleep(5)
            if(adb.checkImg(dev)=='VERIF'):
                deleteChannel(db,account)
                break
            if(not adb.isBan(dev)):
                break
            if(iChannel==0):
                adb.changeName(dev,name)
            adb.stopWhatsapp(dev)
            adb.createFolderTmp(account)
            adb.createBase(account)
            if(pathImage!=""):
                adb.addImageMessage(dev,nums,codes,pathImage,message,account)
            else:
                adb.addMessages(dev,nums,codes,message,account)
            adb.importBase(dev,account)
            time.sleep(0.3)
            adb.startWhatsapp(dev,"")
            timE = time.time()
            while(adb.waitToComplite(dev)!=str(len(nums)) and time.time() - timE < 60):
                screen = adb.checkImg(dev)
                if(screen=='VERIF' or screen=='ROM'):
                    deleteChannel(db,account)
                    fail=True
                    break
                if(adb.isBan(dev)):
                    if(adb.isCrash(dev)):
                        writeInfoNum(dev,db,orderId)
                        continue
                else:
                    setInfoChannel(db,'5',account)
                    writeInfoNum(dev,db,orderId)
                    fail = True
                    break
                time.sleep(0.5)
            if(fail):
                fail=False
                break
            msgCount = writeInfoNum(dev,db,orderId)
            setChannelTotal(db,msgCount,account)
            if(numsIsEmpty):
                break

	
def writeInfoNum(dev,db,orderId):
    outs = adb.readWABase(dev)
    count = len(outs)
    sql = db.cursor()
    for out in outs:
        row = out.split('|')
        if(len(row)==2):
            sql.execute("update order_"+str(orderId)+" set status='"+row[1]+"' where number='"+row[0]+"'")
    db.commit()
    return count



def sendThread(dev,numbers,proxy,codes,message,pathImage,pathAva,name,count_send,orderId):
    db = pymysql.connect(**connection_string)
    send(dev,db,numbers,proxy,codes,message,pathImage,pathAva,name,orderId)
    db.close()

def getColorPixel(dev,x,y):
    dirname = str(time.time())+str(randint(1,99))
    os.mkdir(os.path.join("tmp", dirname))
    sendCommand(dev,"\'screencap -p /sdcard/screen.png\'") 
    runCmd(adb_command+" pull /sdcard/screen.png "+os.path.join("tmp",dirname))
    im = Image.open(os.path.join("tmp",dirname,"screen.png"))
    pix = im.load()
    shutil.rmtree(os.path.join("tmp",dirname))
    return pix[x,y]

class SendThreads(Process):
    def __init__(self,dev,numbers,proxy,codes,message,imagePath,imageAva,nameN,count_send,orderId):
        self.dev = dev
        self.numbers = numbers
        self.proxy = proxy
        self.codes = codes
        self.message = message
        self.imagePath = imagePath
        self.imageAva = imageAva
        self.nameN = nameN
        self.count_send = count_send
        self.orderId = orderId
        Process.__init__(self)
    def run(self):
        sendThread(self.dev,self.numbers,self.proxy,self.codes,self.message,self.imagePath,self.imageAva,self.nameN,self.count_send,self.orderId)

def getProxy():
    proxyList = open("proxy.list", "r")
    ppp = proxyList.read().split("\n")
    return ppp
	
def getChannels(db,dev):
    sql = db.cursor()
    dev = re.findall(r'\.([0-9]{,3}):',dev)
    query = "select number,flag,time_ban from channels where dev='"+dev[0]+"'"
    sql.execute(query)
    channels = []
    for row in sql.fetchall():
        if(row[2]!=None):
            if(time.time()-time.mktime(row[2].timetuple())>86400):
               channels.append(row[0])
        else:
            channels.append(row[0])
    return channels

			
	
def sendOrder():
    db = pymysql.connect(**connection_string)
    proxy = getProxy()
    print len(proxy)
    while(True):
        ############ get order id ##########################
        sql = db.cursor()
        query = "select min(id),name,message,imageava,imagemess,countsend from orders where status='0'";
        sql.execute(query)
        for row in sql.fetchall():
           orderId = row[0]
           name = row[1]
           message = row[2]
           pathAva = row[3]
           pathImage = row[4]
           count_send =  int(row[5])
        ####################################################

        ######## Status order vzyat ########################
        #query = "update orders set status='1' where id='"+str(orderId)+"'";
        #sql.execute(query)
        #db.commit();
        ####################################################
        ##### POLUCHIT DANNUE ZAKAZA #######################
        #sql.execute
        ##########check tables #############################
        query = "show tables";
        sql.execute(query)
        for row in sql.fetchall():
            if (row[0]=="order_"+str(orderId)):
                query = "drop table order_"+str(orderId);
                sql.execute(query)
                db.commit()
        #####################################################
       
        ########### create order table ######################
        query = "create table order_"+str(orderId)+" (id int(10) AUTO_INCREMENT, number varchar(100), code varchar(200),  message varchar(1000), status int(10), PRIMARY KEY (id))"
        sql.execute(query)
        #####################################################

        ########### insert row ##############################
        query = "select numbers,codes from orders where id='"+str(orderId)+"'";
        sql.execute(query)
        for row in sql.fetchall():
           numbers = row[0]
           codes = row[1]
        numbers = numbers.split(';')
        codes = codes.split(';')
        for i in range(len(numbers)):
            query = "insert into order_"+str(orderId)+" (number,code,status) values('"+numbers[i]+"','"+codes[i]+"','0')"
            sql.execute(query)
        db.commit()
        ######################################################
        print "connect devices"
        ############## connect machine #######################
        adb.connect()
        #adb.editGetway("192.168.1.52")         ######айпи ВПНки
        devs = adb.listDev()
        ######################################################

        ###############   Razdelenie nomerov #################
        print str(len(numbers)) + "<--------"
        celoe,ostatok = divmod(len(numbers),len(devs)-1)
        celoe1,ostatok1 = divmod(len(proxy),len(devs)-1)
        ###################

		
        th = range(len(devs))
        ####################
        for i in range(len(devs)):
            count = i + 1
            if(i!=len(devs)-1):
                num2 = count * celoe
                num1 = num2 - celoe
                pr2 = count * celoe1
                pr1 = pr2 - celoe1
                nums = numbers[num1:num2]
                cods = codes[num1:num2]
                prx = proxy[pr1:pr2]
            else:
                num1 = i * celoe
                num2 = num1+ostatok
                pr1 = i * celoe1
                pr2 = pr1+ostatok1
            prx = proxy[pr1:pr2]
            nums = numbers[num1:num2]
            cods = codes[num1:num2]
            th[i] = SendThreads(devs[i],nums,prx,cods,message,pathImage,pathAva,name,count_send,orderId)
            th[i].start()
        while(True):
            time.sleep(1)

        ######################################################


        ################## start threads #####################
        db.close()
        ######################################################
        break


if __name__ == '__main__':
    freeze_support()
    sendOrder()


