import re
import os
import time
import shutil
import sqlite3
import random
from PIL import Image
from random import randint
from subprocess import Popen, PIPE

if os.name=='posix':
    adb_command ='adb'
else:
    import imageHelper
    adb_command ='res\\adb.exe'

def runCmd(cmd):
    print "cmd: ",cmd
    pid_start = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE)
    shell = pid_start.stdout.read()
    print "output cmd: ",shell
    if(len(shell)>2000):
        print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    pid_start.terminate()
    return shell

	
def sendCommand(dev,command):
    dev = dev.replace('\r','')
    command = adb_command+' -s'+dev+' shell "su -c '+command+'"'
    #print command
    out = runCmd(command)
    return out


def listDev():
    machineList = open("devs.list", "r")
    machines = machineList.read().split("\n")
    return machines

def isBan(dev):
    out = sendCommand(dev,"\'dumpsys window windows | grep mCurrentFocus\'")
    if(re.search('com.whatsapp.SpamWarningActivity',out)!=None):
        return False
    else:
        return True

def isCrash(dev):
    out = sendCommand(dev,"\'dumpsys window windows | grep mCurrentFocus\'")
    if(re.search('Application Error:',out)!=None):
        return False
    if(re.search('Application Not Responding:',out)!=None):
        return False
    return True

def cleanChannel(dev):
    sendCommand(dev,"\'pm clear com.whatsapp\'")

def cleanContact(dev):
    sendCommand(dev,"\'pm clear com.android.providers.contacts\'")

def getCurrentAccount(dev):
    out = sendCommand(dev,"\'cat /data/data/com.whatsapp/shared_prefs/com.whatsapp_preferences.xml\'")
    account = re.findall('<string name="registration_jid">(.*)</string>',out)
    return account[0]

def waitWhatsappActive(dev):
    out = sendCommand(dev,"\'dumpsys window windows | grep mCurrentFocus\'")
    while(re.search('com.whatsapp',out)!=None):
        out = sendCommand(dev,"\'dumpsys window windows | grep mCurrentFocus\'")
        time.sleep(0.5)

def waitWhatsappStop(dev):
    out = sendCommand(dev,"\'dumpsys window windows | grep mCurrentFocus\'")
    while(re.search('com.whatsapp',out)==None):
        out = sendCommand(dev,"\'dumpsys window windows | grep mCurrentFocus\'")
 
def startWhatsapp(dev,bol):
    sendCommand(dev,"\'monkey -p com.whatsapp -c android.intent.category.LAUNCHER 1\'")
    if(bol!="no"):
        waitWhatsappActive(dev)
    
def stopWhatsapp(dev):
    sendCommand(dev,"\'am force-stop com.whatsapp\'")

def stopBrowser(dev):
    sendCommand(dev,"\'am force-stop com.android.browser\'")

def connect(dev=""):
    if dev == "":
        devFile = open("devices.dev", "r")
        devs = devFile.read().split("\n")
        for dev in devs:
            runCmd(adb_command+" connect "+dev)
            time.sleep(0.3)
        devsOn = listDev()
        print "-------------- Connected devices ----------------"
        for dev in devsOn:
            print dev + "\n"
        print "-------------------------------------------------"
    else:
        runCmd(adb_command+" connect "+dev)
        print "-------------- Connected devices ----------------"
        print dev + "\n"
        print "-------------------------------------------------"

def connect2():
    devFile = open("autoreg.dev", "r")
    devs = devFile.read().split("\n")
    for dev in devs:
        runCmd(adb_command+" connect "+dev)
        time.sleep(0.3)
    devsOn = listDev()
    print "-------------- Connected devices ----------------"
    for dev in devsOn:
        print dev + "\n"
    print "-------------------------------------------------"
	
def editGetway(host):
    devs =  listDev()
    for dev in devs:
        sendCommand(dev,"\'ip route del default\'")
        time.sleep(1)
        sendCommand(dev,"\'ip route del default\'")
        time.sleep(1)
        sendCommand(dev,"\'ip route add default via "+host+"\'")
        time.sleep(0.3)
		
def editGetway2():
    devFile = open("devices.dev", "r")
    devs = devFile.read().split("\n")
    for dev in devs:
        sendCommand(dev,"\'ip route del default\'")
        time.sleep(1)

def disconnect(dev=''):
    if dev == '':
        runCmd(adb_command+" disconnect")
    else:
        runCmd(adb_command+" disconnect "+dev)

def createVcf(path,numbers):
    fileVcf = open(path,"w")
    for number in numbers:
        fileVcf.write("BEGIN:VCARD\n")
        fileVcf.write("N:"+number+"\n")
        fileVcf.write("TEL;CELL:"+number+"\n")
        fileVcf.write("END:VCARD\n")
    fileVcf.close()

def createFolderTmp(account):
    if(os.path.exists(os.path.join("tmp", account))):
        shutil.rmtree(os.path.join("tmp", account))
    os.mkdir(os.path.join("tmp", account));

def importContacts(dev,numbers,folderName):
    createVcf(os.path.join("tmp",folderName,"contacts.vcf"),numbers)
    runCmd(adb_command+" -s "+dev+" push "+os.path.join("tmp",folderName,"contacts.vcf")+" /sdcard/WApp/"+folderName+"/contacts.vcf")
    runCmd(adb_command+" -s "+dev+' shell am start -t "text/x-vcard" -d file:///sdcard/WApp/'+folderName+'/contacts.vcf -a android.intent.action.VIEW com.android.contacts')
    print dev+" import complite " + str(len(numbers)) + "number"
    
def waitImportContacts(dev,lastNumber):
    count = sendCommand(dev,"\'sqlite3 /data/data/com.android.providers.contacts/databases/contacts2.db \\\"SELECT count(*) FROM data WHERE data1='"+lastNumber+"';\\\"\'")
    count = count.replace("\r","")
    count = count.replace("\n","")
    timer = time.time()
    while(count=="0" or time.time() - timer > 50):
        time.sleep(0.3);
        count = sendCommand(dev,"\'sqlite3 /data/data/com.android.providers.contacts/databases/contacts2.db \\\"SELECT count(*) FROM data WHERE data1='"+lastNumber+"';\\\"\'")
        count = count.replace("\r","")
        count = count.replace("\n","")

def createBase(folderName):
    shutil.copy(os.path.join("data","msgstore.db"), os.path.join("tmp",folderName,"msgstore.db"))

def formatedMessage(message):
    message = message.strip()
    message = message.replace("\r\n", "' || x'0D0A' || '");
    while(re.search('\|\| \'\' \|\|',message)!=None):
        message = message.replace("|| '' ||", "||")
    return message

def addMessages(dev,numbers,codes,message,folderName):
    print "add mesages"
    messageF = formatedMessage(message)
    connect = sqlite3.connect(os.path.join('tmp',folderName,'msgstore.db'))
    c = connect.cursor()
    cj = 0
    for number in numbers:
        number = number.replace("+", "") + "@s.whatsapp.net"
        message = messageF.replace("%code%",codes[cj])
        l1 = int(round(time.time() * 1000))
        l2 = l1 / 1000L
        k = randint(10, 99)
        sql1 = "INSERT INTO messages (key_remote_jid, key_from_me, key_id, status, needs_push, data, timestamp, MEDIA_URL, media_mime_type, media_wa_type, MEDIA_SIZE, media_name , latitude, longitude, thumb_image, remote_resource, received_timestamp, send_timestamp, receipt_server_timestamp, receipt_device_timestamp, raw_data, media_hash, recipient_count, media_duration, origin)VALUES ('"+ number+ "', 1,'"+ str(l2)+ "-"+ str(k)+ "', 0,0, '"+ message+ "',"+ str(l1) + ",'','', '0', 0,'', 0.0,0.0,'','',"+ str(l1)+ ", -1, -1, -1,0 ,'',0,0,0);"
        sql2 = "insert into chat_list (key_remote_jid) select '" + number + "' where not exists (select 1 from chat_list where key_remote_jid='"+ number + "');"
        sql3 = "update chat_list set message_table_id = (select max(messages._id) from messages) where chat_list.key_remote_jid='"+ number + "';" 
        c.execute(sql1)
        connect.commit()
        c.execute(sql2)
        connect.commit()
        c.execute(sql3)
        connect.commit()
        cj = cj+1
    connect.close()
    
def addImageMessage(dev,numbers,codes,pathImage,message,folderName):
    print "add image mesages"
    message = formatedMessage(message)
    connect = sqlite3.connect(os.path.join('tmp',folderName,'msgstore.db'))
    pathDev = imageHelper.createNameImage()
    mediaName = os.path.basename(pathDev)
    imageCaptionF = message
    if not os.name=='posix':
        data = imageHelper.imageGetCode(pathDev,pathImage)
    else:
	data = imageHelper.imageGetCode_py(pathDev,pathImage)
    imageThumb = data[0]
    imageRawData = data[1]
    imageHash = data[2]
    imageSize = data[3]
    runCmd(adb_command+" -s "+dev+" push "+pathImage+' '+pathDev)
    c = connect.cursor()
    cj = 0
    for number in numbers:
        number = number.replace("+", "") + "@s.whatsapp.net"
        imageCaption = imageCaptionF.replace("%code%",codes[cj])
        l1 = int(round(time.time() * 1000))
        l2 = l1 / 1000L
        k = randint(10, 99)
        sql1 = "INSERT INTO messages (key_remote_jid, key_from_me, key_id, status, needs_push, data, timestamp, MEDIA_URL, media_mime_type, media_wa_type, MEDIA_SIZE, media_name , latitude, longitude, thumb_image, remote_resource, received_timestamp, send_timestamp, receipt_server_timestamp, receipt_device_timestamp, raw_data, media_hash, recipient_count, media_duration, origin)VALUES ('"+ number+ "', 1,'"+ str(l2)+ "-"+ str(k)+ "', 0,0, '"+ imageCaption+ "',"+ str(l1) + ",'','', '0', 0,'', 0.0,0.0,'','',"+ str(l1)+ ", -1, -1, -1,0 ,'',0,0,0);"
        sql2 = "insert into chat_list (key_remote_jid) select '" + number + "' where not exists (select 1 from chat_list where key_remote_jid='"+ number + "');"
        sql3 = "update chat_list set message_table_id = (select max(messages._id) from messages) where chat_list.key_remote_jid='" + number + "';"
        c.execute(sql1)
        connect.commit()
        c.execute(sql2)
        connect.commit()
        c.execute(sql3)
        connect.commit()
        cj = cj+1
    connect.close()
    
def importBase(dev,folderName):
    sendCommand(dev,"\'chmod -R 777 /data/data/com.whatsapp\'")
    time.sleep(0.3)
    sendCommand(dev,"\'rm -rf /data/data/com.whatsapp/databases/msgstore.db\'")
    sendCommand(dev,"\'rm -rf /data/data/com.whatsapp/databases/msgstore.db-shm\'")
    sendCommand(dev,"\'rm -rf /data/data/com.whatsapp/databases/msgstore.db-wal\'")
    time.sleep(0.2)
    runCmd(adb_command+" -s "+dev+" push "+os.path.join("tmp",folderName,"msgstore.db")+" /sdcard/msgstore.db")
    sendCommand(dev,"\'cp /sdcard/msgstore.db /data/data/com.whatsapp/databases/msgstore.db\'")
    sendCommand(dev,"\'chmod -R 777 /data/data/com.whatsapp\'")

def changeName(dev,name):
    name = name.replace(" ","_")
    sendCommand(dev,"\'am start -n com.whatsapp/.ProfileInfoActivity\'")
    time.sleep(0.5)
    sendCommand(dev,"\'input swipe 200 200 5 5\'")
    time.sleep(0.3)
    sendCommand(dev,"\'input tap 202 191\'")
    time.sleep(0.2)
    sendCommand(dev,"\'input text "+ name+"\'")
    time.sleep(0.2)
    sendCommand(dev,"\'input tap 179 265\'")

def changeImage(dev,imgOrg):
    runCmd(adb_command+" -s "+dev+" push "+imgOrg+" /sdcard/p1.jpg")
    if(checkImgAva(dev)):
        #sendCommand(dev,"\'input tap 210 141\'")
        time.sleep(0.3)#1
        #sendCommand(dev,"\'input tap 175 171\'")
    else:
        sendCommand(dev,"\'input tap 210 141\'")
        time.sleep(0.2)
        sendCommand(dev,"\'input tap 73 211\'")
        time.sleep(0.2)
        sendCommand(dev,"\'input tap 187 172\'")
        time.sleep(3)


def saveChannel(dev,account):
    if(os.path.exists(os.path.join("channels", account))):
        shutil.rmtree(os.path.join("channels", account))
    os.mkdir(os.path.join("channels", account))
    sendCommand(dev,"\'chmod -R 777 /data/data/com.whatsapp\'")
    sendCommand(dev,"\'rm /data/data/com.whatsapp/databases/msgstore.db-*\'")
    sendCommand(dev,"\'sh /sdcard/WApp/scripts/tar.sh\'")
    runCmd(adb_command+" -s "+dev+" pull /data/data/com.whatsapp/channel.tar "+os.path.join("channels",account))



def restoreChannel(dev,account):
    sendCommand(dev,"\'chmod -R 777 /data/data/com.whatsapp\'")
    sendCommand(dev,"\'rm -r /data/data/com.whatsapp/databases\'")
    sendCommand(dev,"\'rm -r /data/data/com.whatsapp/files\'")
    sendCommand(dev,"\'rm -r /data/data/com.whatsapp/shared_prefs\'")
    sendCommand(dev,"\'rm -r /data/data/com.whatsapp/cache\'")
    time.sleep(0.3)
    runCmd(adb_command+" -s "+dev+" push "+os.path.join("channels",account,"channel.tar")+" /sdcard/")
    sendCommand(dev,"\'cp /sdcard/channel.tar /data/data/com.whatsapp/\'")
    sendCommand(dev,"\'sh /sdcard/WApp/scripts/untar.sh\'")
    sendCommand(dev,"\'rm /data/data/com.whatsapp/channel.tar\'")


def waitToComplite(dev):
    count = sendCommand(dev,"\'sqlite3 /data/data/com.whatsapp/databases/msgstore.db \\\"SELECT count(*) FROM messages WHERE status>='4' and status<'14' and key_from_me='1';\\\"\'")
    count = count.replace("\r","")
    count = count.replace("\n","")
    return count

def readWABase(dev):
    row = sendCommand(dev,"\'sqlite3 /data/data/com.whatsapp/databases/msgstore.db \\\"SELECT key_remote_jid,status FROM messages where key_from_me='1';\\\"\'")
    return row.replace("@s.whatsapp.net","").rstrip().split("\r\r\n")




	
def importImg(path1,path2):
    runCmd(adb_command+" -s "+dev+" push "+path1+' '+path2)

def checkImgOk(dev,igO):
    x1 = 17
    y1 = 155
    imgSave = Image.open(os.path.join("screen", "2.png"))
    w,h = imgSave.size
    igS = imgSave.load()
    count = 0	
    for x in range(w):
        for y in range(h):
            if(igS[x,y]==igO[x+x1,y+y1]):
                count = count + 1
    print str(count) + "=" + str(w*h) + "<---OK"
    if(count == w*h):
        return True
    else:
        return False
   
def checkImgBan(dev,igO):
    x1 = 19
    y1 = 130
    imgSave = Image.open(os.path.join("screen", "1.png"))
    w,h = imgSave.size
    igS = imgSave.load()
    count = 0	
    for x in range(w):
        for y in range(h):
            if(igS[x,y]==igO[x+x1,y+y1]):
                count = count + 1
    if(count == w*h):
        return True
    else:
        return False
	
def checkImgLost(dev,igO):
    x1 = 19
    y1 = 92
    imgSave = Image.open(os.path.join("screen", "8.png"))
    w,h = imgSave.size
    igS = imgSave.load()
    count = 0	
    for x in range(w):
        for y in range(h):
            if(igS[x,y]==igO[x+x1,y+y1]):
                count = count + 1
    #print str(count) + "=" + str(w*h)+"lost"
    if(count == w*h):
        return True
    else:
        return False

def checkImgCall(dev,igO):
    x1 = 14
    y1 = 80
    imgSave = Image.open(os.path.join("screen", "3.png"))
    w,h = imgSave.size
    igS = imgSave.load()
    count = 1	
    for x in range(w):
        for y in range(h):
            if(igS[x,y]==igO[x+x1,y+y1]):
                count = count + 1
    #print str(count) + "=" + str(w*h)
    if(count == w*h):
        return True
    else:
        return False
		
def checkImgWin(dev,igO):
    x1 = 204
    y1 = 223
    imgSave = Image.open(os.path.join("screen", "4.png"))
    w,h = imgSave.size
    igS = imgSave.load()
    count = 0	
    for x in range(w):
        for y in range(h):
            if(igS[x,y]==igO[x+x1,y+y1]):
                count = count + 1
    print str(count) + "=" + str(w*h)
    if(count == w*h):
        return True
    else:
        return False
		
def checkImgConnect(dev,igO):
    x1 = 26
    y1 = 145
    imgSave = Image.open(os.path.join("screen", "12.png"))
    w,h = imgSave.size
    igS = imgSave.load()
    count = 0	
    for x in range(w):
        for y in range(h):
            if(igS[x,y]==igO[x+x1,y+y1]):
                count = count + 1
    #print str(count) + "=" + str(w*h)
    if(count == w*h):
        return True
    else:
        return False
		
def checkImgVerif(dev,igO):
    x1 = 26
    y1 = 101
    imgSave = Image.open(os.path.join("screen", "13.png"))
    w,h = imgSave.size
    igS = imgSave.load()
    count = 0	
    for x in range(w):
        for y in range(h):
            if(igS[x,y]==igO[x+x1,y+y1]):
                count = count + 1
    print str(count+1697) + "=" + str(w*h) + "VERIFITY" + " " + dev
    if(count+1697 == w*h):                     #############ubrat 1697
        return True
    else:
        return False
		

		
def checkImgAva(dev):
    dirname = ''.join([random.choice('0123456789abcdef') for x in range(16)])
    os.mkdir(os.path.join("tmp", dirname))
    print "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
    sendCommand(dev,"\'screencap -p /sdcard/screen.png\'") 
    runCmd(adb_command+" -s "+dev+" pull /sdcard/screen.png "+os.path.join("tmp",dirname))
    print "1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"
    imgScreen = Image.open(os.path.join("tmp",dirname,"screen.png"))
    igO = imgScreen.load()
    x1 = 141
    y1 = 126
    imgSave = Image.open(os.path.join("screen", "11.png"))
    w,h = imgSave.size
    igS = imgSave.load()
    count = 0	
    for x in range(w):
        for y in range(h):
            if(igS[x,y]==igO[x+x1,y+y1]):
                count = count + 1
    print str(count) + "=" + str(w*h)
    if(count == w*h):
        return True
    else:
        return False


def checkImgSer(dev,igO):
    x1 = 188
    y1 = 241
    imgSave = Image.open(os.path.join("screen", "5.png"))
    w,h = imgSave.size
    igS = imgSave.load()
    count = 0	
    for x in range(w):
        for y in range(h):
            if(igS[x,y]==igO[x+x1,y+y1]):
                count = count + 1
    if(count == w*h):
        return True
    else:
        return False
		
def checkImgDalee(dev,igO):
    x1 = 75
    y1 = 245
    imgSave = Image.open(os.path.join("screen", "6.png"))
    w,h = imgSave.size
    igS = imgSave.load()
    count = 0	
    for x in range(w):
        for y in range(h):
            if(igS[x,y]==igO[x+x1,y+y1]):
                count = count + 1
    print str(count) + "=" + str(w*h)
    if(count == w*h):
        return True
    else:
        return False
		
def checkImgBackup(dev,igO):
    x1 = 5
    y1 = 70
    imgSave = Image.open(os.path.join("screen", "7.png"))
    w,h = imgSave.size
    igS = imgSave.load()
    count = 2	
    for x in range(w):
        for y in range(h):
            if(igS[x,y]==igO[x+x1,y+y1]):
                count = count + 1
    #print str(count) + "=" + str(w*h)
    if(count == w*h):
        return True
    else:
        return False
		
def checkImgRom(dev,igO):
    x1 = 27
    y1 = 108
    imgSave = Image.open(os.path.join("screen", "12.png"))
    w,h = imgSave.size
    igS = imgSave.load()
    count = 2	
    for x in range(w):
        for y in range(h):
            if(igS[x,y]==igO[x+x1,y+y1]):
                count = count + 1
    print str(count-2) + "=" + str(w*h) + "ROM"
    if(count-2 == w*h):
        return True
    else:
        return False
		
def checkImg(dev):
    dirname = ''.join([random.choice('0123456789abcdef') for x in range(16)])
    print dirname
    os.mkdir(os.path.join("tmp", dirname))
    sendCommand(dev,"\'rm -f /sdcard/screen.png\'")
    sendCommand(dev,"\'screencap -p /sdcard/screen.png\'") 
    runCmd(adb_command+" -s "+dev+" pull /sdcard/screen.png "+os.path.join("tmp",dirname))
    imgScreen = Image.open(os.path.join("tmp",dirname,"screen.png"))
    igO = imgScreen.load()
    time.sleep(0.3)#1
    if(checkImgOk(dev,igO)):
        #imgScreen.close()
        time.sleep(0.3)#1
        shutil.rmtree(os.path.join("tmp", dirname))
        return "OK"
    if(checkImgBan(dev,igO)):
        #imgScreen.close()
        time.sleep(0.3)#1
        shutil.rmtree(os.path.join("tmp", dirname))
        return "BAN"
    if(checkImgLost(dev,igO)):
        #imgScreen.close()
        time.sleep(0.3)#1
        shutil.rmtree(os.path.join("tmp", dirname))
        return "LOST"
    if(checkImgCall(dev,igO)):
        #imgScreen.close()
        time.sleep(0.3)#1
        shutil.rmtree(os.path.join("tmp", dirname))
        return "CALL"
    if(checkImgWin(dev,igO)):
        #imgScreen.close()
        time.sleep(0.3)#1
        shutil.rmtree(os.path.join("tmp", dirname))
        return "WIN"
    if(checkImgConnect(dev,igO)):
        #imgScreen.close()
        time.sleep(0.3)#1
        shutil.rmtree(os.path.join("tmp", dirname))
        return "CONNECT"
    if(checkImgVerif(dev,igO)):
        #imgScreen.close()
        time.sleep(0.3)#1
        shutil.rmtree(os.path.join("tmp", dirname))	
        return "VERIF"
    if(checkImgRom(dev,igO)):
        #imgScreen.close()
        time.sleep(0.3)#1
        shutil.rmtree(os.path.join("tmp", dirname))
        return "ROM"
    if(checkImgDalee(dev,igO)):
        #imgScreen.close()
        time.sleep(0.3)#1
        shutil.rmtree(os.path.join("tmp", dirname))
        return "DALEE"
    #imgScreen.close()
    time.sleep(0.3)#1
    shutil.rmtree(os.path.join("tmp", dirname))
 		
def changeId(dev):
    id = ''.join([random.choice('0123456789abcdef') for x in range(16)])
    sendCommand(dev,"\'sqlite3 /data/data/com.android.providers.settings/databases/settings.db \\\"UPDATE secure SET value='"+id+"' where name='android_id';\\\"\'")
	
def getIP(dev):
    out = sendCommand(dev,"\'ip addr\'")
    out = re.findall(r'192\.168\.1\.[0-9]{,3}/24',out)
    ip = out[0][0:len(out[0])-3]
    return ip
